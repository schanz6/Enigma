define(['utils/key-codes','rotary'], function(KEY_CODES, Rotary) {

	
	var PositionInput = function(inputId, rotary) {
		
		var self = this;

		this.Rotary = rotary;

		 this.input = $(inputId);

		var getDefaultInputVal = function() {
			for(var i = 0; i< 26; i++)
			{
				var value = String.fromCharCode(i+KEY_CODES.LOWER_CASE_A);
				self.input.append( $("<option>")
				    .val(value)
				    .html(value)
				);
				
			}
		};

		getDefaultInputVal();

		this.resetPosition();


		// add handlers to prevent holding down shift+enter
		self.input.on('change', function() {
			var selected = self.input.find(":selected").text();
			self.Rotary.setPosition(selected.charCodeAt(0)-KEY_CODES.LOWER_CASE_A);
		});

	};

	PositionInput.prototype = {
		
		resetPosition: function()
		{
			this.input.val(String.fromCharCode(this.Rotary.Position + KEY_CODES.LOWER_CASE_A));
		}

	};

	return PositionInput;
});