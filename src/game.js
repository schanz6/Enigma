define([
	'input',
	'rotary',
	'reflector',
	'reset-button',
	'plugs',
	'enigma',
	'utils/key-codes',
	'position-input',
	'plugs-input'
], function(Input, Rotary, Reflector, ResetButton, Plugs, Enigma, KEY_CODES, PositionInput, PlugsInput) {

	var IDs = {
		INPUT: '#input',
		OUTPUT: '#output',
		BUTTON: '#reset-button',
		ROTARY1: '#rotaryOne',
		ROTARY2: '#rotaryTwo',
		ROTARY3: '#rotaryThree',
		KEY_ID: '#keyboard-',
		LIGHT_UP: 'lightUp',
		PLUG_CLASS: '.plug',
		PLUG_ID: '#plug-'
		
	};

	var Game = function() {

		var plugs = new Plugs();
		
		var RotaryOne = new Rotary();
		var RotaryTwo = new Rotary();
		var RotaryThree = new Rotary();
		

		RotaryOne.setPosition(2);
		RotaryTwo.setPosition(14);
		RotaryThree.setPosition(22);

		var Pi1 = new PositionInput(IDs.ROTARY1, RotaryOne);
		var Pi2 = new PositionInput(IDs.ROTARY2, RotaryTwo);
		var Pi3 = new PositionInput(IDs.ROTARY3, RotaryThree);
		

		var enigma = new Enigma([RotaryOne, RotaryTwo, RotaryThree], plugs);

		var runFunc = function(value){
			
			$(IDs.INPUT).append(KEY_CODES.keyCodeToLowerLetter(value));
			var letter = value-KEY_CODES.UPPER_CASE_A;

			letter = enigma.getLetter(letter);

			var output = KEY_CODES.numberToLowerLetter(letter);
			
			  
			 
			$(IDs.OUTPUT).append(output);

			Pi1.resetPosition();
			Pi2.resetPosition();
			Pi3.resetPosition();

			return output;
		};

		var input = new Input(IDs.KEY_ID, IDs.LIGHT_UP,runFunc);
		
		var button = new ResetButton(IDs.BUTTON, IDs.INPUT, IDs.OUTPUT);
		var plugsInput = new PlugsInput(plugs, IDs.PLUG_CLASS, IDs.PLUG_ID);
		
	};


	return Game;
});