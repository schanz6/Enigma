define([], function() {
	var ResetButton = function(buttonId, inputId, outputId) {
		this.el = $(buttonId);

		var self = this;

		this.el.on('click', function() {
			$(inputId).text("");
			$(outputId).text("");

		});
	}

	return ResetButton;
});