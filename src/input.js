define(['utils/key-codes'], function(KEY_CODES) {

	var Input = function(inputName, lightUpClass, runFunc) {
		var self = this;
		var body = $("body");


		// add handlers to prevent holding down shift+enter
		var canRun = true;
		body.on('keydown', function(evt) {

			if (evt.keyCode >= KEY_CODES.UPPER_CASE_A && evt.keyCode <= KEY_CODES.UPPER_CASE_Z) {
				
				//prevents from triggering multiple inputs
				if(canRun)
				{
					canRun = false;
					var resultLetter = runFunc(evt.keyCode);
					$(inputName+resultLetter).addClass(lightUpClass);
				}
				
				
			}
			
			evt.preventDefault();
			
		});

		body.on('keyup', function(evt){
			
			$("."+lightUpClass).removeClass(lightUpClass);
			canRun = true;
		});

	};

	return Input;

});