define([], function() {
	return {

		ENTER: 13,
		LEFT_ARROW: 37,
		UP_ARROW: 38,
		RIGHT_ARROW: 39,
		DOWN_ARROW: 40,
		LOWER_CASE_A: 97,
		UPPER_CASE_A: 65,
		UPPER_CASE_Z: 90,

		keyCodeToNumber: function(keycode)
		{
			return keycode.charCodeAt(0)-this.UPPER_CASE_A;
			
		},

		numberToLowerLetter: function(value)
		{
			return String.fromCharCode(value+this.LOWER_CASE_A);
			
		},

		numberToUpperLetter: function(value)
		{
			return String.fromCharCode(value+this.UPPER_CASE_A);
			
		},

		keyCodeToLowerLetter: function(keycode)
		{
			return String.fromCharCode(keycode-this.UPPER_CASE_A+this.LOWER_CASE_A);
			
		}
	};
});