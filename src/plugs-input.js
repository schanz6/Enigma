define(['utils/key-codes'], function(KEY_CODES) {

	var PlugsInput = function(plugs, inputClass, plugId) {
		var self = this;
		this.Plugs = plugs;
		var plugsUi = $(inputClass);
		
		var selected = "";

		plugsUi.on('click', function(evt) {

			var value = $(this).text().trim();
			
			var oldMatch = KEY_CODES.numberToLowerLetter(self.Plugs.getValue(KEY_CODES.keyCodeToNumber(value)));

			if(value != oldMatch.toUpperCase())
			{
				$(plugId+oldMatch).css("color","black");
				$(plugId+oldMatch).find("circle").css("fill","black");
				self.Plugs.resetLetters(KEY_CODES.keyCodeToNumber(value));
			}

			if(selected === "")
			{
				selected = value
				
				$(this).css("color","darkgray");
				$(this).find("circle").css("fill","darkgray");

			}
			else if (selected === value)
			{
				$(this).css("color","black");
				$(this).find("circle").css("fill","black");
				selected = "";
			}
			else
			{
				self.Plugs.resetLetters(KEY_CODES.keyCodeToNumber(value));
				self.Plugs.switchLetters(KEY_CODES.keyCodeToNumber(value), KEY_CODES.keyCodeToNumber(selected));
				
				var color = self.getRandomColor();
				$(this).css("color",color);
				$(this).find("circle").css("fill",color);
				$(plugId+selected.toLowerCase()).css("color",color);
				$(plugId+selected.toLowerCase()).find("circle").css("fill",color);
				selected = "";

			}
			
		});

		

	};

	PlugsInput.prototype = {
		
		
		getRandomColor: function() {
		    var letters = '0123456789ABCDEF';
		    var color = '#';
		    for (var i = 0; i < 6; i++ ) {
		        color += letters[Math.floor(Math.random() * 16)];
		    }
		    return color;
		}
	}



	return PlugsInput;
});