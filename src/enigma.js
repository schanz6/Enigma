define([
	'rotary',
	'reflector',
	'plugs'
], function(Rotary, Reflector, Plugs) {


	var Enigma = function(Rotarys, Plugs) {

		this.Plugs = Plugs;
		this.Rotarys = Rotarys;

		this.Reflector = new Reflector();

	};

	Enigma.prototype = {
		
		getLetter: function(letter)
		{
			var index = 0;
			var rotary = this.Rotarys[index];
			
			while(rotary.rotate() && ++index < this.Rotarys.length )
			{
				rotary = this.Rotarys[index];

			}

			letter = this.Plugs.getValue(letter);
			
			for (var i = 0; i < this.Rotarys.length; i++) {
			  
			  letter = this.Rotarys[i].getForwardValue(letter);
			
			}

			letter = this.Reflector.getValue(letter);

			for (var i = this.Rotarys.length-1; i >= 0; i--) {
			  
			  letter = this.Rotarys[i].getBackValue(letter);
			
			}

			letter = this.Plugs.getValue(letter);

			return letter;
		},

		 setRotaryPosition: function(index, position)
		 {
		 	this.Rotarys[index].setPosition(position);
		 }

	};



	return Enigma;
});