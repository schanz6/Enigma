define([], function() {

	var Reflector = function() {
		
		var getReflectorMapping = function() {
			return [24,25,3,2,5,4,7,6,9,8,11,10,13,12,15,14,17,16,19,18,21,20,23,22,0,1];
		};

		this.ReflectorMapping = getReflectorMapping();

	};

	Reflector.prototype = {
		
		getValue: function(letter)
		{
			return this.ReflectorMapping[letter];
			
		}

	};

	return Reflector;
});