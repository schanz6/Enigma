define([], function() {

	var Plugs = function() {
		
		var getPlugsMapping = function() {
			return [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
		};

		this.PlugsMapping = getPlugsMapping();
		
	};

	Plugs.prototype = {
		
		switchLetters: function(first, second)
		{
			if(this.PlugsMapping[first] == first && this.PlugsMapping[second] == second)
			{
				this.PlugsMapping[first] = second;
				this.PlugsMapping[second] = first;

				return true;
			}

			return false;
			
		},

		resetLetters: function(letter)
		{
			
			this.PlugsMapping[this.PlugsMapping[letter]] = this.PlugsMapping[letter];
			this.PlugsMapping[letter] = letter;
			
		},

		getValue: function(letter)
		{
			return this.PlugsMapping[letter];
			
		}

	};

	return Plugs;
});