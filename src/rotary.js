define([], function() {

	var Rotary = function() {
		
		var getRotaryMapping = function() {
			return [0,10,2,3,4,5,6,7,8,9,17,11,12,1,14,15,16,13,18,19,20,21,22,23,24,25];
		};

		var getTurnOver = function() {
			return 15;
		};

		this.RotaryMapping = getRotaryMapping();
		this.Position = 0;
		this.TurnOver = getTurnOver();
	};

	Rotary.prototype = {
		
		setPosition: function(position)
		{
			this.Position = position;
		},
		
		getForwardValue: function(letter)
		{
			return this.RotaryMapping[(letter+this.Position)%this.RotaryMapping.length];
			
		},

		getBackValue: function(letter)
		{
			//var position = (letter+this.Position)%this.RotaryMapping.length;
			return (this.RotaryMapping.length+this.RotaryMapping.indexOf(letter)-this.Position)%this.RotaryMapping.length;
			
		},

		rotate: function()
		{
			this.Position = ++this.Position % this.RotaryMapping.length;

			return (this.Position === this.TurnOver);
			
		}

	};

	return Rotary;
});